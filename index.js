angular.module('site').controller('IndexCtrl', function ($rootScope, $state) {
    
    var layoutpage = $('html, body');

    $('.btnroot').click(function () {
        console.log('index btnroot click function');
        layoutpage.stop(true, true).animate ({
            scrollTop: 0
        }, 2000);
    });

    $('.btnhome').click(function () {
        console.log('index btnhome click function');
        layoutpage.stop(true, true).animate({
            scrollTop: $('#row2').offset().top
        }, 1000);
    });

    $('.btncontacts').on('click', function (e){
        if ($state.current.name == 'home') {
            console.log($state.current.name + ' btncontacts if');
            layoutpage.stop(true, true).animate({
                scrollTop: $('#contacts').offset().top
            }, 1000);
        } else {
            $state.go('home');
            console.log('btncontacts else');
            setTimeout(function() {
                $('.btncontacts').click();
            }, 10);
        }
    });

});



