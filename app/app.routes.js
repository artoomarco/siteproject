angular.module('site').config(function ($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
        .state('home', {
            url:'/',
            templateUrl:'../home/home.template.html',
            controller: 'HomeCtrl'
        })
        
        .state('bio', {
            abstract: true,
            url:'/bio',
            templateUrl:'../bio/bio.template.html',
            controller:'BioCtrl'
        })
        
        .state('bio.italian', {
            url:'/italian',
            templateUrl:'../bio/bioitalian.template.html',
            controller:'BioCtrl'
        })
        
        .state('bio.english', {
            url:'/english',
            templateUrl:'../bio/bioenglish.template.html',
            controller:'BioCtrl'
        })
        
        .state('studies', {
            url:'/studies',
            templateUrl:'../studies/studies.template.html',
            controller:'StudiesCtrl'
        })
        
        .state('projects', {
            url:'/projects',
            templateUrl:'../projects/projects.template.html',
            controller:'ProjectsCtrl'
        })
})